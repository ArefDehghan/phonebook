using System.Threading.Tasks;
using PhoneBook.Core;
using PhoneBook.Core.Repositories;

namespace PhoneBook.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PhoneBookContext _context;

        public UnitOfWork(PhoneBookContext context, IPersonRepository people, IPhoneRepository phones)
        {
           _context = context;

            People = people;
            Phones = phones;
        }

        public IPhoneRepository Phones { get; set; }

        public IPersonRepository People { get; set; }

        public int Complete() => _context.SaveChanges();

        public async Task<int> CompleteAsync() => await _context.SaveChangesAsync();

        public void Dispose() => _context.Dispose();

        public async Task DisposeAsync() => await _context.DisposeAsync();
    }
}