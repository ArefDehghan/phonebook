using PhoneBook.Core.Models;
using PhoneBook.Core.Repositories;

namespace PhoneBook.Data.Repositories
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(PhoneBookContext context)
            : base(context)
        {
        }

        public PhoneBookContext PhoneBookContext =>
            Context as PhoneBookContext;
    }
}