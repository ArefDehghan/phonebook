using PhoneBook.Core.Models;
using PhoneBook.Core.Repositories;

namespace PhoneBook.Data.Repositories
{
    public class PhoneRepository : Repository<Phone>, IPhoneRepository
    {
        public PhoneRepository(PhoneBookContext context)
            : base(context)
        {
        }

        public PhoneBookContext PhoneBookContext =>
            Context as PhoneBookContext;
    }
}