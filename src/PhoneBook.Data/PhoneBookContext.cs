using Microsoft.EntityFrameworkCore;
using PhoneBook.Core.Models;

namespace PhoneBook.Data
{
    public class PhoneBookContext : DbContext
    {
        public PhoneBookContext(DbContextOptions<PhoneBookContext> options)
            : base(options)
        {
            
        }

        public DbSet<Person> People { get; set; }

        public DbSet<Phone> Phones { get; set; }
    }
}