using PhoneBook.Core.Models;

namespace PhoneBook.Core.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {
        
    }
}