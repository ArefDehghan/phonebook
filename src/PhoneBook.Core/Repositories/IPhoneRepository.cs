using PhoneBook.Core.Models;

namespace PhoneBook.Core.Repositories
{
    public interface IPhoneRepository : IRepository<Phone>
    {

    }
}