using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhoneBook.Core.Models
{
    public class Person
    {
        public Person()
        {
            Phones = new List<Phone>();
        }

        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string FatherName { get; set; }

        [Required]
        public string Email { get; set; }

        public string Website { get; set; }

        public bool Gender { get; set; }

        public List<Phone> Phones { get; set; }
    }
}