using System.ComponentModel.DataAnnotations;

namespace PhoneBook.Core.Models
{
    public class Phone
    {
        public int Id { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public Person Person { get; set; }
    }
}