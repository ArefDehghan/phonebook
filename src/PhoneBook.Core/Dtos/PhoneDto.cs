using System.ComponentModel.DataAnnotations;

namespace PhoneBook.Core.Dtos
{
    public class PhoneDto
    {
        public int Id { get; set; }

        [Required]
        public string Number { get; set; }

        public int PersonId { get; set; }
    }
}