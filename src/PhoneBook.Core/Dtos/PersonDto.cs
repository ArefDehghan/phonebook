using System.ComponentModel.DataAnnotations;

namespace PhoneBook.Core.Dtos
{
    public class PersonDto
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string FatherName { get; set; }

        [Required]
        public string Email { get; set; }

        public string Website { get; set; }

        public bool Gender { get; set; }
    }
}