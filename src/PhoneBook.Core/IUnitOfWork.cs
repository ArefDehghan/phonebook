using System;
using System.Threading.Tasks;
using PhoneBook.Core.Repositories;

namespace PhoneBook.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IPersonRepository People { get; set; }

        IPhoneRepository Phones { get; set; }

        int Complete();

        Task<int> CompleteAsync();
    }
}